﻿
var drawSquiggle = {
    canvas: '',
    ctx: '',
    flag: true,
    prevX: 0,
    currX: 200,
    prevY: 200,
    currY: 0,
    differenceX: 0,
    differenceY:0,
    dot_flag: true,
    x: "black",
    y: 2,

    init: function () {
        var self = this;
        self.canvas = document.getElementById('can');
        self.ctx = self.canvas.getContext("2d");
        w = self.canvas.width;
        h = self.canvas.height;


    },


    draw: function () {

        var self = this;
        self.ctx.beginPath();
        self.ctx.moveTo(self.prevX, self.prevY);
        self.ctx.lineTo(self.currX, self.currY);
        self.ctx.strokeStyle = self.x;
        self.ctx.lineWidth = self.y;
        self.ctx.fill();
        self.ctx.closePath();
    },

    findxy: function (e) {
        var self = this;
        self.prevX = self.currX;
        self.prevY = self.currY;
       
        self.currX = (e.accelerationIncludingGravity.x * -1) - self.canvas.offsetLeft;
        self.currY = (e.accelerationIncludingGravity.y * -1) - self.canvas.offsetTop;
        self.differenceX = parseFloat(self.prevX - self.currX); 
        self.differenceY = parseFloat(self.prevY - self.currY);
        self.ctx.beginPath();
        self.ctx.fillStyle = "BLACK";
        if (self.differenceX > 0) {
            for (var i = 0; i < self.differenceX; i++) {
                self.ctx.fillRect(i, self.currY, 10, 10);
                self.prevX = self.currX;
                self.currX = (e.accelerationIncludingGravity.x * 10) - self.prevX;
                self.draw();
            }
        } else {
            for (var i = 0; i > self.differenceX; i--) {
                self.ctx.fillRect(i, self.currY, 10, 10);
                self.prevX = self.currX;
                self.currX = (e.accelerationIncludingGravity.x * 10) - self.prevX;
                self.draw();
            }
        }
        if (self.differenceY > 0) {
            for (var i = 0; i < self.differenceY; i++) {
                self.ctx.fillRect(self.currX, i, 10, 10);
                self.prevY = self.currY;
                self.currY = (e.accelerationIncludingGravity.y * 10) - self.prevY;
                self.draw();
            }
        } else {
            for (var i = 0; i > self.differenceY; i--) {
                self.ctx.fillRect(self.currX, i, 10, 10);
                self.prevY = self.currY;
                self.currY = (e.accelerationIncludingGravity.y * 10) - self.prevY;
                self.draw();
            }
        }


    }
}