﻿/*
 * Simple javascript mobile OS / iphone/ android detector
 *
 * License: GPLv3+
 * Author: justin.kelly.org.au
 *
 */
var phoneDetec = {

    deviceIphone: "iphone",
    deviceIpod: "ipod",
    devicePalm: "palm",
    deviceS60: "series60",
    deviceSymbian: "symbian",
    engineWebKit: "webkit",
    deviceAndroid: "android",
    deviceWinMob: "windows ce",
    deviceWinPhone: "windows phone",
    deviceBB: "blackberry",
    uagent : navigator.userAgent.toLowerCase(),
    
         
    
    //**************************
    // Detects if the current device is an iPhone.
    DetectIphone: function () {
        var self = this;
        if (uagent.search(self.deviceIphone) > -1)
            return true;
        else
            return false;
    },

    //**************************
    // Detects if the current device is an iPod Touch.
    DetectIpod: function () {
        var self = this;
        if (uagent.search(self.deviceIpod) > -1)
            return true;
        else
            return false;
    },

    //**************************
    // Detects if the current device is an iPhone or iPod Touch.
    DetectIphoneOrIpod: function () {
        var self = this;
        if (self.DetectIphone())
            return true;
        else if (self.DetectIpod())
            return true;
        else
            return false;
    },

    //**************************
    // Detects if the current browser is the S60 Open Source Browser.
    // Screen out older devices and the old WML browser.
    DetectS60OssBrowser: function () {
        var self = this;
        if (self.uagent.search(self.engineWebKit) > -1) {
            if ((self.uagent.search(self.deviceS60) > -1 ||
                 self.uagent.search(self.deviceSymbian) > -1))
                return true;
            else
                return false;
        }
        else
            return false;
    },



    //**************************
    // Detects if the current device is an Android OS-based device.
    DetectAndroid: function () {
        var self = this;
        if (self.uagent.search(self.deviceAndroid) > -1)
            return true;
        else
            return false;
    },


    //**************************
    // Detects if the current device is an Android OS-based device and
    //   the browser is based on WebKit.
    DetectAndroidWebKit: function () {
        var self = this;
        if (self.DetectAndroid()) {
            if (self.DetectWebkit())
                return true;
            else
                return false;
        }
        else
            return false;
    },


    //**************************
    // Detects if the current browser is a Windows Mobile device.
    DetectWindowsMobile: function () {
        var self = this;
        if (self.uagent.search(self.deviceWinMob) > -1)
            return true;
        else
            return false;
    },

    //**************************
    // Detects if the current browser is a Windows Phone OS 7+ Mobile device.
    DetectWindowsPhone: function () {
        var self = this;
        if (self.uagent.search(self.deviceWinPhone) > -1)
            return true;
        else
            return false;
    },

    //**************************
    // Detects if the current browser is a BlackBerry of some sort.
    DetectBlackBerry: function () {
        var self = this;
        if (self.uagent.search(self.deviceBB) > -1)
            return true;
        else
            return false;
    },

    //**************************
    // Detects if the current browser is on a PalmOS device.
    DetectPalmOS: function () {
        var self = this;
        if (self.uagent.search(self.devicePalm) > -1)
            return true;
        else
            return false;
    },

    DetectMobileOS: function () {
        var self = this;
        if (self.DetectIphoneOrIpod() || self.DetectS60OssBrowser() || self.DetectAndroid() || self.DetectAndroidWebKit() || self.DetectWindowsMobile() || self.DetectBlackBerry() || self.DetectPalmOS()) {
            return true
        } else {
            return false
        }

    }
}